import { HitController } from './controllers/hit.controller';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HitService } from './hit.service';
import { Hit, HitSchema } from './models/hit.model';
import HitRepository from './hit.repository';

@Module({
  imports: [MongooseModule.forFeature([{ name: Hit.name, schema: HitSchema }])],
  controllers: [HitController],
  providers: [HitService, HitRepository],
})
export class HitModule {}
