import { Controller, Delete, Get, Param, Res } from '@nestjs/common';
import { Response } from 'express';
import HitRepository from '../hit.repository';
import { HitDocument } from '../models/hit.model';

@Controller('hits')
export class HitController {
  constructor(private readonly repository: HitRepository) {}

  @Get()
  async index(@Res() res: Response): Promise<Response<HitDocument[]>> {
    return this.repository.index(res);
  }

  @Delete(':id')
  async destroy(@Res() res: Response, @Param('id') id: string): Promise<void> {
    return this.repository.destroy(res, id);
  }
}
