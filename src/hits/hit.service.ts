import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';
import axios from 'axios';
import { Model } from 'mongoose';
import { Hit, HitDocument } from '../hits/models/hit.model';

@Injectable()
export class HitService {
  constructor(@InjectModel(Hit.name) private hitModel: Model<HitDocument>) {}
  private readonly logger = new Logger(HitService.name);

  @Cron('*/1 * * * *')
  async handleCron() {
    const list = await this.hitModel.find();
    if (list.length === 0) {
      axios
        .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .then((res) => {
          const { data } = res;
          const { hits } = data;
          this.hitModel.deleteMany({}).exec();

          hits.map((hit: HitDocument) => {
            this.hitModel.create(hit);
          });
        });
      this.logger.debug(
        'Ha sido actualizado la base de datos con nuevos registros.',
      );
    }
  }
}
